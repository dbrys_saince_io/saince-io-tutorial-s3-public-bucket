# Tutorial - Using Python and Public datasets on AWS S3

This repo contains a single python notebook that you can use in Google Colabs or in Jupyter.

More information on [Google Colabs](https://colab.research.google.com/)

Just download the notebook and upload it in Colabs or Jupyter.

Have fun!